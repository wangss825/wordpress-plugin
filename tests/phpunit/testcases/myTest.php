<?php
use Brain\Monkey;
use \Brain\Monkey\Functions;
use Brain\Monkey\Tests\FunctionalTestCase;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;

class SomeClassTest extends \PluginTestCase {

    public function testAddAndHas() {
		    add_action('wp_enqueue_scripts', function ( ) { return true; });
		    static::assertTrue(has_action('wp_enqueue_scripts', 'function( )'));
			  static::assertFalse(has_action('foo', 'function()'));
	 }
		public function testAddAndHasWithoutCallback()
    {
        static::assertFalse(has_action('wp_enqueue_scripts'));
        add_action('wp_enqueue_scripts', [$this, __FUNCTION__], 20);
        static::assertTrue(has_action('wp_enqueue_scripts'));
   }
	 public function testAddShortcodeWhen() {
		    Functions\when('add_shortcode')
			    ->justReturn(true);
			  static::assertTrue(add_shortcode(1));

	 }
	 public function testAddShortcodeExpect(){
		    Functions\expect('add_shortcode')
					->once()
					->with(1)
					->andReturn(False);
		    static::assertFalse(add_shortcode(1));
	 }
 }

?>
