<?php

/*
Plugin Name: get-data
Description: Plugin for the creating the api for get-data from json
Author: Dan Liu
Text Domain: get-data
Version: 1.0

*/
##Below is just for safety to check if the the ABSPATH and add_action exist.

#Check to see if the global constant 'ABSPATH'- Absolute Path  is defined
if ( ! defined( 'ABSPATH' ) ){
    exit;
}
/**
Function to show the user click end point and the event when user click.
*/
function users_table_handle(){
    $get_users="<a href='#' id='users'><strong>Users</strong></a>";
    $users_table = '';
    $users_table .="<table id='users_table'>";
    $users_table .="<thead><tr><th>ID</th><th>Name</th><th>User Name</th></tr></thead>";
    $users_table .="</table>";

    echo $get_users;
    echo $users_table;


    $user_details= "<div id='user_details'><p><strong>User details</strong></p>";
    $user_details .="<div id='userid'></div>";
    $user_details .="<div id='name'></div>";
    $user_details .="<div id='username'></div>";
    $user_details .="<div id='email'></div>";
    $user_details .="<div id='address'>Address:</div>";
    $user_details .="<div></div>";
    $user_details .="<div id='suite'></div>";
    $user_details .="<div id='city'></div>";
    $user_details .="<div id='zipcode'></div>";
    $user_details .="<div id='geo'>Geo:</div>";
    $user_details .="<div id='lat'></div>";
    $user_details .="<div id='lng'></div>";
    $user_details .="<div id='phone'></div>";
    $user_details .="<div id='website'></div>";
    $user_details .="<div>Company:</div>";
    $user_details .="<div id='company_name'></div>";
    $user_details .="<div id='catchPhrase'></div>";
    $user_details .="<div id='bs'></div>";
    $user_details .="</div>";
    echo $user_details;
?>
<script>
jQuery(document).ready( function($) {
    jQuery("#users").click( function() {
    $('#users_table').css("display", 'block');
    var user_table = $('#users_table').DataTable({
        "ajax": {
         type : "GET",
         url : "https://jsonplaceholder.typicode.com/users/",
         dataSrc:"",
         error:function(request, status, error){
             alert(request.responseText);
         }
       },
          "bSort": true,
          "bPaginate" : true,
          "bProcessing" : true,
          "pageLength" : 100,
          "info": false,
          "deferRender": true,
          "lengthChange": false,
          "searching": false,
          "paging": false,
          "scrollY": "400px",
          "scrollCollapse": true,
          "autoWidth": true,
          "columns": [
               { data: 'id'},
               { data: 'name'},
               { data: 'username'}
           ],
           "columnDefs": [
            {
               "targets": 0,
               "render": function ( data, type, row, meta ) {
                return '<a href="#" class='+ meta.row +'>'+data+ '</a>';
                }
            },
            {
               "targets": 1,
               "render": function ( data, type, row, meta ) {
                return '<a href="#" class='+ meta.row + '>'+data+'</a>';
              }
            },
            {
               "targets": 2,
               "render": function ( data, type, row, meta ) {
                return '<a href="#" class='+ meta.row + '>'+data+ '</a>';
              }
            }
          ],
            "rowCallback": function( td, data, index ){
                //$(row).css('<a>' + data + '</a>');
                //$(td).addClass('selected');
            }
          });
        $('#users').css('pointer-events',"none");

//Show the user details information when click one user.
        $('#users_table' ).on( 'click', "a",  function () {
        if($('#user_details').css("display", 'none')){
            $('#user_details').css("display", 'block');
        }
        var index = $(this).attr("class");
           //console.log(index);
            index++;
            var URL = "https://jsonplaceholder.typicode.com/users/"+index;
           //console.log(URL);
            var user_details = $.ajax({
                url:URL,
                type: "GET",
                dataType: 'json',
                success:function(result){
                   //console.log(result);
             },
                 error:function(request, status, error){
                      alert(request.responseText);
             }
           }).done(function(data){
                 $('#userid').html("<span>ID: "+ data.id+ "</span");
                 $('#name').html("<span>Name: "+ data.name+ "</span");
                 $('#username').html("<span>Username: "+ data.username+ "</span");
                 $('#email').html("<span>Email: "+ data.email+ "</span");
                 $('#street').html("<span>Street:"+ data.address.street+ "</span");
                 $('#suite').html("<span>Suite:"+ data.address.suite+ "</span");
                 $('#city').html("<span>City:"+ data.address.city+ "</span");
                 $('#zipcode').html("<span>Zipcode:"+ data.address.szipcode+ "</span");
                 $('#lat').html("<span>Latitude:"+ data.address.geo.lat+ "</span");
                 $('#lng').html("<span>Longitude:"+ data.address.geo.lng+ "</span");
                 $('#phone').html("<span>Phone:"+ data.phone+ "</span");
                 $('#website').html("<span>Website:"+ data.website+ "</span");
                 $('#company_name').html("<span>Company Name:"+ data.company.name+ "</span");
                 $('#catchPhrase').html("<span>CatchPhrase:"+ data.company.catchPhrase+ "</span");
                 $('#bs').html("<span>Bs: "+ data.company.catchPhrase+ "</span");
           })

        });

    });

   });

   </script>
<?php
 }
 add_shortcode("end_point",'users_table_handle');

 /**
 Function to add the style sheet and js
 */

function get-data_scripts() {
    //add jquery in case some version doesn't support.
    wp_enqueue_script( 'jquery' );
    //css
    wp_register_style( 'get-data_css',  plugins_url('css/get-data.css', __FILE__) );
    wp_enqueue_style( 'get-data_css' );
    //js
    wp_enqueue_script( 'datatable_js',  plugins_url('js/datatables.js', __FILE__) );
    wp_enqueue_script( 'datatable_js');
 }
 add_action('wp_enqueue_scripts', 'get-data_scripts');

 ?>
