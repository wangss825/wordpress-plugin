## Project  
* Contributors: Dan Liu
* Tags: Plugin used for the get data from json
* Requires PHP: 7.3
* License: See LICENSE for details
* Vision: 1.0


## Project Title
get-data plugin and Brain Monkey test.

## Description
* This plugin request for data on the https://jsonplaceholder.typicode.com/ api and inserts it into a custom table users_table for local use. The user can get more details info from this api when using another ajax request and show the results below the table.

* The unit test used for this plugin is Brain Monkey.

## Requirements
* WordPress
* Sql database
* Composer


## Installation and usage instruction

This section describes how to install the plugin and get it working.

* Navigate to the '/wp-content/plugins/' folder in your wordpress platform.
* Type 'git clone https://wangss825@bitbucket.org/wangss825/wordpress-plugin.git'
* Activate the plugin through the 'Plugins' screen in WordPress.
* Use the Settings->Plugin Name screen to configure the plugin
* Under wordpress try a new page or whatever you want to put. Enter the shortcode[end_point]. Then open the page. You will find the user link.

##  Running the tests

This section describe how to install the test and get it working.

* Open one terminal. Navgation to '/wp-content/plugins/get-data' folder.
* Run 'composer install'.
* Run 'vendor/bin/phpunit' to run the test.

## Explanations behind non-obvious implementation choices
*  You can use composer to manage you folder whatever you prefer
*  In this project, I used the datatable to implement the table. Because it is convenient and fast.  and There are also other methods such as plain php and html to draw the table.

## Composer
* The composer package is a tool that you can manage your project folder. Such as the wordpress folder. There are a lot of files under the root folder. If you change it. There will be error. If you use composer. You can arrange your folder tree such as you can put your code under src folder,  and clean all  the other files. This will not broke the platform.
* You can use composer to download and instal a lot of packages. Just add under the composer.json.
* After create composer.json file. Under the same folder. Enter some package. Use 'composer install' command. It will produce a folder called vendor. It will include the package you need. also it will create composer.lock file.


## Caching
* Method
Under chrome browser, use developer mode->network-> dont't check "disable cache" select box.
Then from the header tag-> response headers-> you will find the "cache-control" is max-ag = 3153600 or some number very big. And the expires date is a future date. Otherwise, from the request headers it will show no cache.
*  Why we use cache.
Whenever when you make a request from the api. It have to go to the remote server to fetch the data. It will take some time according the network situation. If you use cache. It will store the data you fetch before in you local machine. So it will make the action more quickly.
